import { api } from '@/api'

export const uploadStaticAsset = async (file, apiBase, progressCallback) => {
  const formData = new FormData()
  formData.append('file', file)

  const apiResponse = await api.upload('static-assets', formData, { base: apiBase }, progressCallback)
  return apiResponse.data
}


const iconMixin = {
  props: {
    color: {
      type: String,
      defaulit: 'trsText',
    },
    size: {
      type: Number,
      default: 16,
    },
  },

  data: function () {
    return {
    }
  },

  methods: {
  },

  computed: {
  },
}

export default iconMixin
